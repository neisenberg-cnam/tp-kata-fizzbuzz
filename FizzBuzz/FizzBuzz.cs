﻿namespace FizzBuzz;

public class FizzBuzz
{
    public static string Generer(int n)
    {
        var number = new IntValue(n);
        return RecursiveGenerate(number.number);
    }

    private static string RecursiveGenerate(int n)
    {
        if (n <= 1)
            return GetStringFromANumber(n);
        else
            return RecursiveGenerate(n-1) + GetStringFromANumber(n);
    }

    public static string GetStringFromANumber(int n)
    {
        string res = "";
        if (n % 3 == 0)
            res += "Fizz";
        if (n % 5 == 0)
            res += "Buzz";
        else if (n % 3 != 0 && n % 5 != 0)
            return n.ToString();
        return res;
    }
}