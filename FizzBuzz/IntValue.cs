namespace FizzBuzz;

public class IntValue
{
    public int number { get; set; }

    public IntValue(int n)
    {
        if (n < 15)
            throw new InvalidInputException("Number should be above 15");
        else if (n > 150)
            throw new InvalidInputException("Number should be under 150");
        else
            number = n;
    }
}