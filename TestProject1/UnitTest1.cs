using Xunit;
using FizzBuzz;
using Xunit.Sdk;
using static FizzBuzz.FizzBuzz;

namespace TestProject1;

public class UnitTest1
{
    [Fact]
    public void basicFizzBuzz()
    {
        string result = FizzBuzz.FizzBuzz.GetStringFromANumber(1);
        Assert.Equal("1", result);
        string resultFizz = FizzBuzz.FizzBuzz.GetStringFromANumber(3);
        Assert.Equal("Fizz", resultFizz);
        string resultBuzz = FizzBuzz.FizzBuzz.GetStringFromANumber(5);
        Assert.Equal("Buzz", resultBuzz);
        string resultFizzBuzz = FizzBuzz.FizzBuzz.GetStringFromANumber(15);
        Assert.Equal("FizzBuzz", resultFizzBuzz);
    }

    [Fact (Skip = "Used for development, should throw error now")]
    public void FizzBuzzInferiorToMin()
    {
        string resultFizz = FizzBuzz.FizzBuzz.Generer(3);
        Assert.Equal("12Fizz", resultFizz);
        string resultBuzz = FizzBuzz.FizzBuzz.Generer(5);
        Assert.Equal("12Fizz4Buzz", resultBuzz);
    }

    [Fact]
    public void FizzBuzzTest()
    {
        string result = FizzBuzz.FizzBuzz.Generer(16);
        Assert.Equal("12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz16", result);
    }

    [Fact]
    public void FizzBuzzErrors()
    {
        Assert.Throws<InvalidInputException>(() => FizzBuzz.FizzBuzz.Generer(4));
        Assert.Throws<InvalidInputException>(() => FizzBuzz.FizzBuzz.Generer(151));
    }
    
}